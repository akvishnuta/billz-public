<?php
    class Home extends CI_Controller{
        public function index(){
            if($this->session->userdata('username')!=''){
                $data['title'] = 'Main Page';
                $this->load->view('common/commonHeaders');
                $this->load->view('common/commonTopBar');
                $this->load->view('common/commonSplash');
                $this->load->view('index',$data);
            }else{
                $this->session->set_flashdata('redir_page','home');
                redirect('login');
            }
        }
    }
?>