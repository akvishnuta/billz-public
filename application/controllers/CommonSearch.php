<?php
    class CommonSearch extends CI_Controller{
        public function doLoad(){
            $this->load->model('CommonSearch_Model');
            $dataObj = json_decode($this->input->post('dataString'),true);
            $result = json_encode($this->CommonSearch_Model->doLoad($dataObj));
            echo $result;
        }
    }
?>