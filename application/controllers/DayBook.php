<?php
    class DayBook extends CI_Controller{
        public function index(){
            if($this->session->userdata('username')!=''){
                $data['title'] = 'DayBook';
                $this->load->view('common/commonHeaders');
                $this->load->view('common/commonTopBar');
                $this->load->view('common/commonSplash');
                $this->load->view('common/commonModal');
                $this->load->view('daybook',$data);
            }else{
                $this->session->set_userdata('redir_page','daybook');
                redirect('login');
            }
        }

        public function doLoad(){
            $this->load->model('DayBook_Model');
            $dataObj = json_decode($this->input->post('dataString'),true);
            $dataMap = $this->DayBook_Model->doLoad($dataObj);
            $openingBal='';
            $data=array();
            if($dataMap){
                $data['dataObj'] = $dataMap;
                $data['status'] = "Success";
                $opening = $this->DayBook_Model->getOpening($dataObj['dateFrom']);
                $openingBal=$debTot=$credTot=$i=0;
                foreach ($opening as $row){
                    if($i==0){
                        $credTot = $row->credTot;
                        $debTot = $row->debTot;
                        $i++;
                    }
                }
                $openingBal = (int)$debTot-(int)$credTot;
            }else{
                if(count($dataMap)==0)
                    $data['status'] = "";
                else
                    $data['status'] = "Failed";
            }
            $data['openingBal'] = $openingBal;
            echo json_encode($data);
            $this->db->close();
        }
        public function loadAreas(){
            $this->load->model('Area_Model');
            $data = $this->Area_Model->loadAreas();
            if($data){
                echo json_encode($data);
            }else{
                echo 'Failed';
            }
            $this->db->close();
        }
        public function loadPartyList(){
           $this->load->model('PartyMaster_Model');
            $dataMap = $this->PartyMaster_Model->doLoad();
            if($dataMap){
                echo json_encode($dataMap);
            }else{
                echo 'Failed';
            }
            $this->db->close();
        }
    }
?>