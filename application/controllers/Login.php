<?php
    class Login extends CI_Controller{
        public function index(){
            $data['title'] = 'Login User';
            $this->load->view('common/commonHeaders');
            $this->load->view('common/commonSplash');
            $this->load->view('login',$data);
        }
        public function loadAreas(){
            $this->load->model('Area_Model');
            $data = $this->Area_Model->loadAreas();
            echo json_encode($data);
            $this->db->close();
        }
        public function checkUser(){
            $dataString = json_decode($this->input->post('dataString'),true);
            $username = $dataString['username'];
            $password = $dataString['password'];
            $areaCode = $dataString['areaCode'];
            $result=NULL;
            $this->load->model('User_Model');
            $data = $this->User_Model->getPassword($username);
            $password_from_db = $data[0]->password;
            $salt = $data[0]->salt;
            $privilege = $data[0]->privilege;
            $phone = $data[0]->phone;
            $areaCodeDb=$data[0]->areaCode;
            $this->load->model('Area_Model');
            $data=$this->Area_Model->loadArea($areaCode);
            $count = count($data);
            $this->load->library('PasswordHasher');
            $checkResult=$this->passwordhasher->checkPassword($password,$salt,$password_from_db);
            if($checkResult && $count>0 &&($areaCode==$areaCodeDb || $areaCodeDb==-1)){
                $area = $data[0]->area;
                $result['status']='true';
                $session_data = array(
                    'username'=>$username,
                    'area'=>$area,
                    'privilege'=>$privilege,
                    'phone'=>$phone
                );
                $this->session->set_userdata($session_data);
                $redir_page = $this->session->userdata('redir_page');
                if($redir_page!=''){
                    $result['redir_url'] = $redir_page;
                }else{
                    $result['redir_url'] = '';
                }
               echo json_encode($result);
            }else{
                $result['status'] = 'false';
                echo json_encode($result);
            }
            $this->db->close();
        }
        public function logout(){
            $this->session->unset_userdata('username');
            $this->session->unset_userdata('area');
            redirect('login');
        }
    }
?>