<?php
    class PartyMaster extends CI_Controller{
        public function index(){
            if($this->session->userdata('username')!=''){
                $data['title'] = 'PartyMaster';
                $this->load->view('common/commonHeaders');
                $this->load->view('common/commonTopBar');
                $this->load->view('common/commonSplash');
                $this->load->view('common/commonModal');
                $this->load->view('partyMaster',$data);
            }else{
                $this->session->set_userdata('redir_page','partyMaster');
                redirect('login');
            }
        }
        
        public function addNew(){
            $this->load->model('PartyMaster_Model');
            echo $this->PartyMaster_Model->getNextCode();
            $this->db->close();
        }
        
        public function doSave(){
            $this->load->model('PartyMaster_Model');
            $dataString = json_decode($this->input->post('dataString'),true);
            $partyCode = $this->PartyMaster_Model->getNextCode();
            $partyName = $dataString['partyName'];
            $partyMark = $dataString['partyMark'];
            $partyAddress = $dataString['partyAddress'];
            $gstNo = $dataString['gstNo'];
            $this->load->model('PartyMaster_Model');
            $dataMap = array();
            if($this->PartyMaster_Model->doSave($partyCode,$partyName,$partyMark,$partyAddress,$gstNo)){
                $dataMap['status']='Saved';
                $dataMap['partyCode'] = $partyCode;
                $dataMap['partyName'] = $partyName;
                $dataMap['partyAddress'] = $partyAddress;
                $dataMap['gstNo'] = $gstNo;
            }else{
                $dataMap['status']='Failed';
            }
            echo json_encode($dataMap);
            $this->db->close();
        }
        public function doLoad(){
            $this->load->model('PartyMaster_Model');
            $dataMap = $this->PartyMaster_Model->doLoad();
            echo json_encode($dataMap);
             $this->db->close();
        }
    }
?>