<?php
    class PhpInfo extends CI_Controller{
        public function index(){
            if($this->session->userdata('username')!=''){
                $data['title'] = 'phpinfo';
                $this->load->view('phpinfo',$data);
            }else{
                $this->session->set_userdata('redir_page','phpInfo');
                redirect('login');
            }
        }
   }
?>