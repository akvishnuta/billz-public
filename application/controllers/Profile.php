<?php
    class Profile extends CI_Controller{
        public function index(){
            $data=array();
            if($this->session->userdata('username')!=''){

                    $data['title'] = 'Register';
                    $data['headMessage'] = 'Oops! You ran into an Exception.';
                    $data['message'] = 'This page is under construction!';
                    $this->load->view('common/commonHeaders');
                    $this->load->view('common/commonTopBar');
                    $this->load->view('common/commonSplash');
                    $this->load->view('common/commonModal');
                    //$this->load->view('profile',$data);
                    $this->load->view('error',$data);
            }else{
                $this->session->set_userdata('redir_page','profile');
                redirect('login');
            }
        }
        
        public function loadAreas(){
            $this->load->model('Area_Model');
            $data = $this->Area_Model->loadAreas();
            echo json_encode($data);
            $this->db->close();
        }
        
        public function loadProfile(){
            //$this->load->helper('url');
            $username = $this->session->userdata('username')?$_SESSION["username"]:'Error';
            $this->load->model('Profile_Model');
            $data = $this->Profile_Model->loadProfile($username);
            $data[0]->imgurl = base_url().'uploads/profile/'.$data[0]->imgurl;
            echo json_encode($data);
            $this->db->close();
        }
        public function doUpload(){
            $this->deleteExisting();
            $config['upload_path']   = './uploads/profile'; 
            $config['allowed_types'] = 'gif|jpg|png'; 
            $config['max_size']      = 1024*5; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;  
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
               $error = array('error' => $this->upload->display_errors());  
            }else { 
               $data = array('upload_data' => $this->upload->data()); 
            } 
            echo json_encode($data);
        }
        public function doSaveImgUrl(){
            $this->load->model('Profile_Model');
            $dataString = json_decode($this->input->post('dataString'),true);
            $filename = $dataString['filename'];
            $username = $dataString['username'];
            $dataMap = array();
            if($this->Profile_Model->doSaveImgUrl($username,$filename)){
                $dataMap['status']='Saved';
                $dataMap['filename'] = $filename;
                $dataMap['username'] = $username;
            }else{
                $dataMap['status']='Failed';
            }
            echo json_encode($dataMap);
            $this->db->close();
        }
        public function deleteExisting(){
            $this->load->model('Profile_Model');
            $username = $this->session->userdata('username')?$_SESSION["username"]:'Error';
            $data = $this->Profile_Model->loadProfile($username);
            $data[0]->imgurl = './uploads/profile/'.$data[0]->imgurl;
            $imgurl = $data[0]->imgurl;
            unlink($imgurl);
        }
        public function doSave(){
            $this->load->model('Register_Model');
            $dataString = json_decode($this->input->post('dataString'),true);
            $sl = $this->Register_Model->getNextCode();
            $username = $dataString['username'];
            $password = $dataString['password'];
            $phone = $dataString['phone'];
            $areaCode = $dataString['areaCode'];
            $hashData=$this->hashPassword($password);
            $salt = $hashData['salt'];
            $password = $hashData['hash'];
            $this->load->model('Register_Model');
            $dataMap = array();
            if($this->Register_Model->doSave($sl,$username,$password,$salt,$phone,$areaCode)){
                $dataMap['status']='Saved';
                $dataMap['partyCode'] = $username;
                $dataMap['phone'] = $phone;
                $dataMap['areaCode'] = $areaCode;
            }else{
                $dataMap['status']='Failed';
            }
            echo json_encode($dataMap);
            $this->db->close();
        }
        
        private function hashPassword($password){
            $hashData = array();
            $this->load->library('PasswordHasher');
            $hashData = $this->passwordhasher->hashPassword($password);
            return $hashData;
        }
    }
?>