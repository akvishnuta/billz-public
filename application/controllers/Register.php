<?php
    class Register extends CI_Controller{
        public function index(){
            $data=array();
            if($this->session->userdata('username')!=''){
                if($this->session->userdata('privilege')=='1'){
                    $data['title'] = 'DayBook';
                    $this->load->view('common/commonHeaders');
                    $this->load->view('common/commonTopBar');
                    $this->load->view('common/commonSplash');
                    $this->load->view('common/commonModal');
                    $this->load->view('register',$data);
                }else{
                    $data['title'] = 'Error';
                    $data['headMessage'] = 'Access denied';
                    $data['message'] = 'Sorry! You dont have enough privilege to access the contents of this page';
                    $this->load->view('error',$data);
                }
            }else{
                $this->session->set_userdata('redir_page','register');
                redirect('login');
            }
        }
        
        public function loadAreas(){
            $this->load->model('Area_Model');
            $data = $this->Area_Model->loadAreas();
            echo json_encode($data);
            $this->db->close();
        }
        
        public function doSave(){
            $this->load->model('Register_Model');
            $dataString = json_decode($this->input->post('dataString'),true);
            $sl = $this->Register_Model->getNextCode();
            $username = $dataString['username'];
            $password = $dataString['password'];
            $phone = $dataString['phone'];
            $areaCode = $dataString['areaCode'];
            $hashData=$this->hashPassword($password);
            $salt = $hashData['salt'];
            $password = $hashData['hash'];
            $this->load->model('Register_Model');
            $dataMap = array();
            if($this->Register_Model->doSave($sl,$username,$password,$salt,$phone,$areaCode)){
                $dataMap['status']='Saved';
                $dataMap['partyCode'] = $username;
                $dataMap['phone'] = $phone;
                $dataMap['areaCode'] = $areaCode;
            }else{
                $dataMap['status']='Failed';
            }
            echo json_encode($dataMap);
            $this->db->close();
        }
        
        private function hashPassword($password){
            $hashData = array();
            $this->load->library('PasswordHasher');
            $hashData = $this->passwordhasher->hashPassword($password);
            return $hashData;
        }
    }
?>