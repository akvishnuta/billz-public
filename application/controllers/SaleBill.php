<?php
    class SaleBill extends CI_Controller{
        public function index(){
            if($this->session->userdata('username')!=''){
                $data['title'] = 'SaleBill';
                $this->load->view('common/commonHeaders');
                $this->load->view('common/commonTopBar');
                $this->load->view('common/commonSplash');
                $this->load->view('common/commonModal');
                $this->load->view('common/commonSearch');
                $this->load->view('saleBill',$data);
            }else{
                $this->session->set_userdata('redir_page','saleBill');
                redirect('login');
            }
        }
        
        public function addNew(){
            $this->load->model('SaleBill_Model');
            echo $this->SaleBill_Model->getNextCode();
            $this->db->close();
        }
        public function loadItems(){
            $this->load->model('SaleBill_Model');
            echo json_encode($this->SaleBill_Model->loadItems());
            $this->db->close();
        }
        public function doSave(){
            $this->load->model('SaleBill_Model');
            $dataString = json_decode($this->input->post('dataString'),true);
            $billNo = $this->SaleBill_Model->getNextCode();
            $partyName = $dataString['partyName'];
            $partyCode = $dataString['partyCode'];
            $date = $dataString['date'];
            $total = $dataString['total'];
            $gridList = $dataString['gridList'];
            $this->load->model('SaleBill_Model');
            $dataMap = array();
            if($this->SaleBill_Model->doSave($billNo,$partyName,$partyCode,$date,$total,$gridList)){
                $dataMap['status']='Saved';
                $dataMap['billNo'] = $billNo;
                $dataMap['partyName'] = $partyName;
            }else{
                $dataMap['status']='Failed';
            }
            echo json_encode($dataMap);
            $this->db->close();
        }
    }
?>