<?php
    class Voucher extends CI_Controller{
        public function index(){
            if($this->session->userdata('username')!=''){
                $data['title'] = 'Main Page';
                $this->load->view('common/commonHeaders');
                $this->load->view('common/commonTopBar');
                $this->load->view('common/commonSplash');
                $this->load->view('common/commonSearch');
                $this->load->view('common/commonModal');
                $this->load->view('voucher',$data);
            }else{
                $this->session->set_userdata('redir_page','voucher');
                redirect('login');
            }
        }
        public function addNew(){
            $this->load->model('Voucher_Model');
            echo $this->Voucher_Model->getNextCode();
            $this->db->close();
        }
        public function loadPartyCombo(){
            $this->load->model('PartyMaster_Model');
            $dataMap = $this->PartyMaster_Model->doLoad();
            echo json_encode($dataMap);
             $this->db->close();
        }
        
        public function loadItemCombo(){
            $this->load->model('ItemMaster_Model');
            $dataMap = $this->ItemMaster_Model->doLoad();
            echo json_encode($dataMap);
            $this->db->close();
        }
        
        public function loadParticulars(){
            $this->load->model('Particulars_Model');
            $dataMap = $this->Particulars_Model->doLoad();
            echo json_encode($dataMap);
            $this->db->close();
        }
         public function doSave(){
            $this->load->model('Voucher_Model');
            $dataString = json_decode($this->input->post('dataString'),true);
            $vno = $this->Voucher_Model->getNextCode();
            $dataObj = array(
                'vno' => $vno,
                'date1' => $dataString['date'],
                'partyCode' => $dataString['partyCode'],
                'particulars' => $dataString['particulars'],
                'credit' => $dataString['credit'],
                'debit' => $dataString['debit'],
                'total'=> ((int)$dataString['debit']-(int)$dataString['credit']),
                'led_narration' => $dataString['led_narr'],
                'day_narration' => $dataString['day_narr'],
                'mark' => $dataString['RorP'],
                'login'=>$this->session->userdata('username'),
                'area'=>$this->session->userdata('area'),
                'dea_name'=>$dataString['partyName']
            );
            
            
            $this->load->model('Voucher_Model');
            $dataMap = array();
            if($this->Voucher_Model->doSave($dataObj)){
                $dataMap['status']='Saved';
                $dataMap['vno'] = $vno;
                $dataMap['date']=$dataString['date'];
                $dataMap['partyName'] = $dataString['partyName'];
            }else{
                $dataMap['status']='Failed';
            }
            echo json_encode($dataMap);
            $this->db->close();
        }
    }
?>