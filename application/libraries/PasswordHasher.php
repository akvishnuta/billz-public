<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PasswordHash
 *
 * @author Akhil
 */
class PasswordHasher{
    private $Blowfish_Pre = '';
    private $Blowfish_End = '';
    private $Allowed_Chars ='';
    private $Chars_Len = 0;
    private $Salt_Length = 0;
    //put your code here
    public function __construct() {
       $this->Blowfish_Pre = '$2a$05$';
       $this->Blowfish_End = '$';
       $this->Allowed_Chars ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./';
       $this->Chars_Len = 63;
       $this->Salt_Length = 21;
    }
    
    public function hashPassword($password){
        $hashData = array();
        $salt='';
        CRYPT_BLOWFISH or die ('No Blowfish found.');
        
        for($i=0; $i<$this->Salt_Length; $i++)
        {
            $salt .= $this->Allowed_Chars[mt_rand(0,$this->Chars_Len)];
        }
        $bcrypt_salt = $this->Blowfish_Pre .$salt . $this->Blowfish_End;
        $hashed_password = crypt($password, $bcrypt_salt);
        $hashData['hash']=$hashed_password;
        $hashData['salt']=$salt;
        return $hashData;
    }
    
    public function checkPassword($password,$salt,$hash){
        CRYPT_BLOWFISH or die ('No Blowfish found.');
        $hashed_pass = crypt($password, $this->Blowfish_Pre.$salt.$this->Blowfish_End);
        if($hash==$hashed_pass){
            return true;
        }else{
            return false;
        }
    }
    
}
