<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Area_Model
 *
 * @author Akhil
 */
class Area_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function loadAreas(){
        $this->db->select(array('code','area'));
        $this->db->from('area');
        $query=$this->db->get();
        $data=$query->result();
        return $data;
    }
    
    public function loadArea($areaCode){
        $this->db->select('area');
        $this->db->from('area');
        $this->db->where('code',$this->db->escape_like_str($areaCode));
        $query=$this->db->get();
        $data=$query->result();
        return $data;
    }
}
