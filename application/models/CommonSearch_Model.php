<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Area_Model
 *
 * @author Akhil
 */
class CommonSearch_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();   
    }
    
    public function doLoad($dataMap){
        $allowedTables=array("partymaster","item_mas","particulars");
        
        $table = $dataMap['table'];
        $colList = $dataMap['colList'];
        $pattern = $dataMap['pattern'];
        $compareCol = $dataMap['compareCol'];
        if(in_array($table, $allowedTables)){
            $this->db->select($colList);
            $this->db->from($table);
            $this->db->where($compareCol." like '".$this->db->escape_like_str($pattern)."%'");
            $query=$this->db->get();
            $data=$query->result();
            return $data;  
        }else{
            return 'Failed';
        }
        
    }
}
