<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Model
 *
 * @author Akhil
 */
class DayBook_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function doLoad($dataObj){
       $dateFrom = $dataObj['dateFrom'];
       $dateTo = $dataObj['dateTo'];
       $area = $dataObj['area'];
       $whereClause='';
       $this->db->select(array('date1 as Date','day_narration as Narration','credit as Credit','debit as Debit','mark',
                                'login as Login','area as Area','vno as VNo','partyCode'));
       $this->db->from('voucher'); 
       if($area=!"All"){
           $whereClause = 'area='.$this->db->escape_like_str($area).' and ';
       }
       $whereClause = $whereClause.'(date1 between "'.$this->db->escape_like_str($dateFrom).'" and "'.$this->db->escape_like_str($dateTo).'")';
       $this->db->where($whereClause);
       $query=$this->db->get();
       $data=$query->result();
       return $data;
    }
    
    public function getOpening($dateFrom){
        $this->db->select('sum(credit) as credTot, sum(debit) as debTot');
        $this->db->from('voucher');
        $this->db->where('date1<"'.$this->db->escape_like_str($dateFrom).'"');
        $query=$this->db->get();
        $data=$query->result();
        return $data;
    }
}

?>