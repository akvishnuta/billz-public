<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Model
 *
 * @author Akhil
 */
class ItemMaster_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function doLoad(){
       $this->db->select(array('itemCode','itemName','rate','mark'));
       $this->db->from('item_mas'); 
       $query=$this->db->get();
       $data=$query->result();
       return $data;
    }
}

?>