<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Model
 *
 * @author Akhil
 */
class PartyMaster_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getNextCode(){
        $this->db->select('max(partyCode) as code');
        $this->db->from('partymaster');
        $query = $this->db->get();
        $data=$query->result();
        return $data[0]->code+1;
    }
    
    public function doSave($partyCode,$partyName,$partyMark,$partyAddress,$gstNo){
        $data = array(
            'partyCode'=>$partyCode,
            'partyName'=>$partyName,
            'partyMark'=>$partyMark,
            'partyAddress'=>$partyAddress,
            'gstNo'=>$gstNo
        );
        return $this->db->insert('partymaster',$data);
    }
    public function doLoad(){
       $this->db->select(array('partyCode','partyName','partyMark','partyAddress','gstNo'));
       $this->db->from('partymaster'); 
       $query=$this->db->get();
       $data=$query->result();
       return $data;
    }
}

?>