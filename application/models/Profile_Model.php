<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Area_Model
 *
 * @author Akhil
 */
class Profile_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function loadProfile($username){
        $this->db->select(array('sl','name','username','phone','areaCode','privilege','imgurl','email'));
        $this->db->from('users');
        $this->db->where('username',$username);
        $query=$this->db->get();
        $data=$query->result();
        return $data;
    }
    public function doSaveImgUrl($username,$filename){
        $this->db->where('username',$username);
        return $this->db->update('users',array('imgurl'=>$filename));
    }
    public function loadAreas(){
        $this->db->select(array('code','area'));
        $this->db->from('area');
        $query=$this->db->get();
        $data=$query->result();
        return $data;
    }
    
    public function loadArea($areaCode){
        $this->db->select('area');
        $this->db->from('area');
        $this->db->where('code',$this->db->escape_like_str($areaCode));
        $query=$this->db->get();
        $data=$query->result();
        return $data;
    }
    
    public function doSave($sl,$username,$password,$salt,$phone,$areaCode){
        $privilege=0;
        $data = array(
            'sl'=>$sl,
            'username'=>$username,
            'password'=>$password,
            'salt'=>$salt,
            'phone'=>$phone,
            'areaCode'=>$areaCode,
            'privilege'=>$privilege
        );
        return $this->db->insert('users',$data);
    }
    
    public function getNextCode(){
        $this->db->select('max(sl) as code');
        $this->db->from('users');
        $query = $this->db->get();
        $data=$query->result();
        return $data[0]->code+1;
    }
}
