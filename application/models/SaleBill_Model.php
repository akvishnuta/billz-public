<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Model
 *
 * @author Akhil
 */
class SaleBill_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getNextCode(){
        $this->db->select('max(billNo) as billNo');
        $this->db->from('salebillmas');
        $query = $this->db->get();
        $data=$query->result();
        return $data[0]->billNo+1;
    }
    
    public function doSave($billNo,$partyName,$partyCode,$date,$total,$gridList){
        $result=FALSE;
        $data = array(
            'billNo'=>$billNo,
            'partyCode'=>$partyCode,
            'date1'=>$date,
            'total'=>$total,
            'login'=>$this->session->userdata('username'),
            'area'=>$this->session->userdata('area')
        );
        $this->db->trans_begin();
        $result = $this->db->insert('salebillmas',$data);
        if($result){
            for($row=0;$row<count($gridList);$row++){
                $gridList[$row]['billNo']=$billNo;
                $gridRow = $gridList[$row];
                unset($gridRow['sl']);
                $result = $this->db->insert('salebillchild',$gridRow);
                if(!$result)
                    break;
            }
        }
        $this->db->trans_complete();
        return $result;     
    }
    
    public function loadItems(){
       $this->db->select(array('itemCode','itemName','rate'));
       $this->db->from('item_mas'); 
       $query=$this->db->get();
       $data=$query->result();
       return $data;
    }
}

?>