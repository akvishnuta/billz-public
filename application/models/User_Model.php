<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Model
 *
 * @author Akhil
 */
class User_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getPassword($username){
        $this->db->select(array('password','salt','areaCode','privilege','phone'));
        $this->db->from('users');
        $this->db->where('username',$this->db->escape_like_str($username));
        $query = $this->db->get();
        $data=$query->result();
        return $data;
    }
}

?>