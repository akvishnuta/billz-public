<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Area_Model
 *
 * @author Akhil
 */
class Voucher_Model extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
   public function getNextCode(){
        $this->db->select('max(vno) as vno');
        $this->db->from('voucher');
        $query = $this->db->get();
        $data=$query->result();
        return $data[0]->vno+1;
   }
    public function doSave($dataObj){
        return $this->db->insert('voucher',$dataObj);
    }
}
