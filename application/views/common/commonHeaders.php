<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<base href="<?php echo base_url();?>" />
<link href="<?php echo base_url();?>css/common/fa-css/fontawesome-all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap/bootstrap.css">
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/common/common.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/common/fa-css/fontawesome-all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/common/jsgrid/jsgrid-theme.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/common/jsgrid/jsgrid.css">

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script type = 'text/javascript' src = "<?php echo base_url();?>js/bootstrap/bootstrap.js"></script>
<script type = 'text/javascript' src = "<?php echo base_url();?>js/common/jsgrid/jsgrid.js"></script>
</html>