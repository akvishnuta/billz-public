<style>
 #common-search-dynamic-table .clickable-row{
    cursor: pointer;
  }
  #common-search-dynamic-table .active{
    background-color:#2015F7;
    color:#fff;
  }
</style>
<script src="<?php echo base_url();?>js/common/commonSearch.js"></script>
<div class="modal fade modal-md" id="common-search-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="common-search-title">Search</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="common-search-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fab fa-sistrix"></i></span>
              </div>
              <input type="text" class="form-control" id="searchText" placeholder="Search" aria-describedby="inputGroupPrepend2">
            </div>
          </div>
          <div class="col-lg-12">
            <!--<p id="common-search-content">-->
            <div id="common-search-grid" class="table-responsive" style="overflow-x:hidden"></div>
            <!--</p>-->
          </div>
        </div>
      </div>
      <div id="common-search-footer" class="modal-footer">
        <span id="row-count"></span>
        <button hidden type="button" id="common-search-ok-btn" class="btn btn-primary" data-dismiss="modal">Add Anyway</button>
      </div>
    </div>
  </div>
</div>
