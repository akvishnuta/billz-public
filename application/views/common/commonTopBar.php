<style>
  .ui-topbar{
      text-decoration:none;
      color: #9fcdff;
  }
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 top-bar">
			<div class="row">
				<div class="col-lg-3 col-sm-12 text-left">
                                  <span> Hi!<?php 
					$username = $this->session->userdata('username')?$_SESSION["username"]:'Error'; 
					echo ucfirst($username);  
					?></span>&nbsp;&nbsp;&nbsp;
					<span>You are logged in to </span>
					<?php
						$areaName = $this->session->userdata('area'); 
						echo '<strong>'.$areaName.'</strong> Locality';
					?>
				</div>
				<div class=" offset-lg-7 col-sm-12 col-lg-2 text-right">
                                  <a href="profile" class="ui-topbar"><i class="fas fa-user"></i>My Profile</a>
                                        &nbsp;&nbsp;&nbsp;
					<a href="login/logout" class="ui-topbar"><i class="fas fa-sign-out-alt"></i> Log out</a>
				</div>

			</div>
			<div class="row">
					<div class="col-lg-1 col-sm-6">
                                            <span>
                                                    <a href="home" class="ui-topbar"><i class="fas fa-home"></i>Main Menu</a>
                                            </span>
					</div>
                                        
				</div>
		</div>
	</div>
</div>