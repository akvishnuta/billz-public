<html>
	<head>
		<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/daybook/daybook.css">
                <script type = 'text/javascript' src = "<?php echo base_url();?>js/daybook/daybook.js"></script>
	</head>
	<body>
		<div class="container">
			<br><br>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<h1>Day Book</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<hr>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					
					<div class="card">
						<div class="card-body">
							<div class="row">
							  <div class="col-lg-4 col-sm-6 col-xs-6 form-group">
							   <label for="Date From">Date From</label>
							    <input type="date" class="form-control" id="date-from" placeholder="Date">
							  </div>
							  <div class="col-lg-4 col-sm-6 col-xs-6 form-group">
							   <label for="Date To">Date To</label>
							    <input type="date" class="form-control" id="date-to" placeholder="Date">
							  </div>
							  <div class="col-lg-4 col-sm-12 form-group">
							   <label for="locality">Locality</label>
							    <select type="text" class="form-control" id="area" placeholder="Locality"></select>
							  </div>
							</div>
							<div class="row">

							  <div class="form-group col-lg-4">
							   <button id="btn-load" class="btn btn-primary">Load</button>
							  </div>
							  <div class="offset-lg-4 col-lg-4 col-sm-12 form-group form-inline">
							  	<div class="row">
								   <label class="col-sm-7 text-right" for="openingBal">Opening Bal.(+)</label>
								    <input type="text" enabled="false" class="col-sm-5 form-control" id="openingBal" placeholder="Opening Bal.">
								</div>
							   </div>
							  <span class="badge badge-danger" id="error-label"></span>
							 </div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<div id="daybook-grid" class="table-responsive"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					
					<div class="card">
						<div class="card-body">
							<div class="row">
								
								<div class="col-lg-3 col-sm-12 form-group">
								   <label for="totalDebit">Total Debit(+)</label>
								    <input type="text" enabled="false" class="form-control" id="totalDebit" placeholder="Total Debit">
								  </div>
									<div class="col-lg-3 col-sm-12 form-group">
								   <label for="totalCredit">Total Credit(-)</label>
								    <input type="text" enabled="false" class="form-control" id="totalCredit" placeholder="Total Credit">
								  </div>							  
								  <div class="col-lg-3 col-sm-12 form-group">
								   <label for="total">(=)Balance Total</label>
								    <input type="text" enabled="false" class="form-control" id="total" placeholder="Total">
								  </div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</body>
</html>