<html>
	<head>
            <title><?php echo $title;?></title>
            <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/login/login.css">
            <script type = 'text/javascript' src = "<?php echo base_url();?>js/login/login.js"></script>

	</head>
	<body>
		<div class="container">
			<br><br>
			<div class="row">
				<div class="heading col-sm-12 col-lg-12">
					<h1><?php echo $headMessage;?></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<hr style="border-color: #fff;">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-4 offset-lg-4">
					
					<div class="card">
						<div class="card-body">
							<form>
								<div id="login-div" class="row">
									<div class="col-sm-12 form-group">
										<span id="login-error"><?php echo $message;?></span>
									</div>
									
									
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>