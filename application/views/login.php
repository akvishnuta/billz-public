<html>
	<head>
                <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/login/login.css">
                <script type = 'text/javascript' src = "<?php echo base_url();?>js/login/login.js"></script>

	</head>
	<body>
		<div class="container">
			<br><br>
			<div class="row">
				<div class="heading col-sm-12 col-lg-4">
					<h1>Login</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<hr style="border-color: #fff;">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-4 offset-lg-4">
					
					<div class="card">
						<div class="card-body">
							<form>
								<div id="login-div" class="row">
									<div class="col-sm-12 form-group">
										<span id="login-error"></span>
									</div>
									<div class="col-sm-12 form-group">
									    <label  for="item">UserName</label>
									    <input type="text" enabled="false" class="form-control" id="username" placeholder="UserName">
									</div>
									<div class="col-sm-12 form-group">
									    <label  for="item">Password</label>
									    <input type="password" enabled="false" class="form-control" id="password" placeholder="Password">
									</div>
									<div class="col-sm-12 form-group">
									    <label  for="area">Area</label>
									    <select class="form-control" id="area">
									    	
									    </select>
									</div>
									<div class="col-sm-12 form-group text-right">
									    <button id="sign-in-btn" class="btn btn-success">Sign In</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>