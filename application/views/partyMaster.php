<html>
	<head>
		<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/partyMaster/partyMaster.css">
                <script type = 'text/javascript' src = "<?php echo base_url();?>js/partyMaster/partyMaster.js"></script>
	</head>
	<body>
          <div class="container">
			<br><br>
			<div class="row">
				<div class="col-sm-12 col-lg-4">
					<h1>Party Master</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-4">
					
					<div class="card">
						<div class="card-body">
							<form>
							  <div class="form-group">
							    <label  for="partyCode">Party Code</label>
							    <input type="text" enabled="false" class="form-control" id="partyCode" placeholder="Code">
							  </div>
							  <div class="form-group">
							    <label for="partyName">Party Name</label>
							    <input type="text" class="form-control" id="partyName" placeholder="Party Name">
							  </div>
                                                          <div class="form-group">
							    <label for="partyAdddress">Party Address</label>
							    <input type="text" class="form-control" id="partyAddress" placeholder="Party Address">
							  </div>
                                                          <div class="form-group">
							    <label for="gstNo">GST No.</label>
							    <input type="text" class="form-control" id="gstNo" placeholder="GST No.">
							  </div>
							  <div class="form-group">
							    <label for="partyMark">Party Mark</label>
							    <select class="form-control" id="partyMark">
							    	<option value="P">P</option>
							    	<option value="S">S</option>
							    </select>
							  </div>
							  <div class="form-group">
							   <button id="btn-save" class="btn btn-primary">Save</button>
							  </div>
							  <span class="badge badge-danger" id="error-label"></span>
							</form>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-lg-8">
                                  <div style="overflow: auto" id="party-grid"></div>
                                         <!--class="table-responsive"-->
				</div>
			</div>
		</div>
	</body>
</html>