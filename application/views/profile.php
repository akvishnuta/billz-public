<html>
	<head>
                <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/profile/profile.css">
                <script type = 'text/javascript' src = "<?php echo base_url();?>js/common/jquery.ajaxfileupload.js"></script>
                <script type = 'text/javascript' src = "<?php echo base_url();?>js/profile/profile.js"></script>

	</head>
	<body>
		<div class="container">
                  <br><br>
			<div class="row">
				<div class="col-sm-12 col-lg-4">
					<h1>Profile</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<hr style="border-color: #fff;">
				</div>
                            
			</div>
                    
                        
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                          <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#basic-tab">Basic</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#area-tab">Change Area</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#password-tab">Change Password</a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <section class="tab-pane container card active" id="basic-tab">
                                                <div class="card-body">
							<form>
                                                            <div id="login-div" class="row">
                                                              <div class="col-lg-6">
                                                                    <div class="media">
                                                                        <img src="<?php echo base_url();?>\uploads\profile\default.png" id="profile-photo" class="align-self-start mr-3" style="width:60px">
                                                                        <div class="media-body">
                                                                          <h4 id="name-display">Loading...</h4>
                                                                          <p id="username-display">please wait!</p>
                                                                          <form action="profile/do_upload">
                                                                            <input hidden name="userfile" type="file" enabled="false" id="file" placeholder="filename">
                                                                          </form>
                                                                          <div class="fa fa-upload" id="change-btn" style="cursor:pointer;color:#0056b3;"> Change Picture</div>
                                                                        </div>
                                                                      </div>
                                                                    <div class="col-lg-12 form-group">
                                                                            <span id="login-error"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <label  for="name">Name</label>
                                                                        <input type="text" enabled="false" class="form-control" id="name" placeholder="Name">
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <label  for="phone">Phone</label>
                                                                        <input type="text" enabled="false" class="form-control" id="phone" placeholder="Phone">
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <label  for="email">Email</label>
                                                                        <input type="text" enabled="false" class="form-control" id="email" placeholder="Email">
                                                                    </div>
                                                                    
                                                              </div>
                                                            </div>
							</form>
						</div>
                                            </section>
                                            <section class="tab-pane container fade card" id="area-tab">
                                              <div class="card-body">
							<form>
                                                            <div id="login-div" class="row">
                                                                    <div class="col-sm-12 form-group">
                                                                            <span id="login-error"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-lg-6 form-group">
									    <label  for="area">Area</label>
									    <select class="form-control" id="area">  	
									    </select>
									</div>
								</div>
							</form>
						</div>
                                          </section>
                                            <section class="tab-pane container card fade" id="password-tab">
                                              <div class="card-body">
							<form>
                                                            <div id="login-div" class="row">
                                                              <div class="col-lg-6 col-sm-12">
                                                                    <div class="col-sm-12 form-group">
                                                                            <span id="login-error"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <label  for="current-password">Current Password</label>
                                                                        <input type="password" enabled="false" class="form-control" id="current-password" placeholder="Current-Password">
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <label  for="new-password">New Password</label>
                                                                        <input type="password" enabled="false" class="form-control" id="new-password" placeholder="New-Password">
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <label  for="retype-password">Retype Password</label>
                                                                        <input type="password" enabled="false" class="form-control" id="retype-password" placeholder="Retype Password">
                                                                    </div>
                                                              </div>     
								</div>
							</form>
						</div>
                                              
                                          </section>
                                        </div>
				</div>
			</div>
                  <div class="row">
                        <div class="col-lg-2 offset-lg-10 form-group">
                            <br>
                            <button id="update-btn" class="btn btn-success form-control">Update</button>
                        </div>
                    </div>
		</div>
	</body>
</html>