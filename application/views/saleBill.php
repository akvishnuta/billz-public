<html>
	<head>
		<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/saleBill/saleBill.css">
                <script type = 'text/javascript' src = "<?php echo base_url();?>js/saleBill/saleBill.js"></script>
	</head>
	<body>
		<div class="container">
			<br><br>
			<div class="row">
				<div class="col-sm-12 col-lg-4">
					<h1>Sale Bill</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					
					<div class="card">
						<div class="card-body">
							<div class="row">
							  <div class="col-lg-4 col-sm-12 form-group">
							    <label  for="partyCode">Bill No</label>
							    <input type="text" disabled class="form-control" id="billNo" placeholder="billNo">
							  </div>
							  <div class="col-lg-6 col-sm-12 form-group">
							    <label for="partyName">Party Name</label>
							    <div class="row">
									<div class="col-lg-12 col-md-12">
										<div class="input-group">
										<div class="input-group-prepend">
							                <span id="party-search" class="btn btn-primary input-group-text"><i class="fab fa-sistrix"></i></span>
							             </div>
									    <input type="text" class="form-control col-lg-9" id="partyName" placeholder="Party Name">
									    <input type="text" enabled="false" class="form-control col-lg-2" id="partyCode" placeholder="Code">
										</div>

									</div>
								</span>
								</div>
							  </div>
							  <div class="col-lg-2 col-sm-12 form-group">
							    
							    <!-- <input type="text" enabled="false" class="form-control" id="partyCode" placeholder="Code"> -->
							  </div>
							  <div class="col-lg-4 col-sm-12 form-group">
							   <label for="partyMark">Date</label>
							    <input type="date" enabled="false" class="form-control" id="date" placeholder="Date">
							  </div>
							  <div class="col-lg-4 col-sm-12 form-group">
							   <button id="btn-save" style="margin-top: 35px;" class="btn btn-success">Save</button>
							  </div>
							  <div class="col-lg-2 offset-lg-10 col-sm-12"><span class="badge badge-danger" id="error-label-main"></span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-1 col-sm-3 form-group">
								    <label for="slNo">Sl No</label>
								    <input disabled type="text" enabled="false" class="form-control" id="slNo" placeholder="slNo">
								</div>
								<div class="col-lg-3 col-sm-9 form-group">
								    <label  for="item">Item</label>
								    <div class="input-group">
								    	<div class="input-group-prepend">
							                <span id="item-search" class="btn btn-primary input-group-text"><i class="fab fa-sistrix"></i></span>
							             </div>
								    	<select type="text" class="form-control item" id="item" placeholder="item"></select>
								    </div>
								</div>
								<div class="col-lg-2 col-sm-6 form-group">
								    <label  for="qty">Qty</label>
								    <input type="number" class="form-control qty" id="qty" placeholder="Qty">
								</div>
								<div class="col-lg-2 col-sm-6 form-group">
								    <label  for="rate">Rate</label>
								    <input type="number" class="form-control rate" id="rate" placeholder="Rate">
								</div>
								<div class="col-lg-2 col-sm-8 form-group">
								    <label  for="item">Total</label>
								    <input type="text" disabled class="form-control total" id="total" placeholder="Total">
								</div>
								<div class="col-lg-2 col-sm-4 form-group">
								    <button style="margin-top: 35px;" id="add-btn" class="btn btn-primary">Add + </button>

								</div>
								<div class="col-lg-2 offset-lg-9 col-sm-12"><span class="badge badge-danger" id="error-label"></span></div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<div id="sale-grid" class="table-responsive">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Sl No.</th>
											<th>Item</th>
											<th>Qty</th>
											<th>Rate</th>
											<th>Total</th>
											<th></th>
										</tr>
									</thead>
									<tbody id="dynamic-body">
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
                        <br>
			<div class="row">
				<div class="col-lg-2 text-center offset-lg-6 col-sm-4 form-group">
				    <label  for="item"><h4>Grand Total : </h4></label>					
				</div>
				<div class="col-lg-3 col-sm-8 form-group">
				    <input type="text" disabled class="form-control" id="grand-total" placeholder="Grand Total">
				</div>
			</div>

		</div>

		<div class="modal fade modal-md" id="edit-modal" tabindex="-1" role="dialog">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="edit-modal-title">Edit Item</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body" id="edit-modal-body">
		       <div class="container">
		       	<div class="row">
		       		<div class="col-sm-12 form-group">
					    <label  for="slNo">Sl No</label>
					    <input disabled type="number" enabled="false" class="form-control slNo" id="edit-slNo" placeholder="slNo">
					</div>
					<div class="col-sm-12 form-group">
					    <label  for="item">Item</label>
					    <select disabled type="text" class="form-control item" id="edit-item" placeholder="item"></select>
					</div>
					<div class="col-sm-12 form-group">
					    <label  for="qty">Qty</label>
					    <input type="number" class="form-control qty" id="edit-qty" placeholder="Qty">
					</div>
					<div class="col-sm-12 form-group">
					    <label  for="rate">Rate</label>
					    <input type="number" class="form-control rate" id="edit-rate" placeholder="Rate">
					</div>
					<div class="col-sm-12 form-group">
					    <label  for="item">Total</label>
                                            <input type="text" disabled class="form-control total" id="edit-total" placeholder="Total">
					</div>
		       	</div>
		       </div>
		      </div>
		      <div id="edit-modal-footer" class="modal-footer">
		        <button type="button" id="edit-ok-btn" class="btn btn-secondary" data-dismiss="modal">OK</button>
		      </div>
		    </div>
		  </div>
		</div>
	</body>
</html>