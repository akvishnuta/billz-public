<html>
	<head>
		<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/voucher/voucher.css">
                <script type = 'text/javascript' src = "<?php echo base_url();?>js/voucher/voucher.js"></script>
	</head>
        <body>
		<div class="container">
			<br><br>
			<div class="row">
				<div class="col-sm-12 col-lg-4">
					<h1>Voucher</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					
					<div class="card">
						<div class="card-body">
							<div class="row">
								
								<div class="col-lg-3 col-sm-12">
									<div class="row">
										<div class="form-group col-lg-12">
										    <label for="voucherNo">Voucher No</label>
                                                                                    <input type="text" disabled class="form-control" id="voucherNo" placeholder="Voucher No">
										  </div>
									</div>
									<div class="row">
										<div class="form-group col-lg-12">
										    <label for="date">Date</label>
										    <input type="date" class="form-control" id="date">
										  </div>
									</div>
								</div>
								<div class="col-lg-3 col-sm-12">
									<div class="row">
										<div class="form-group col-lg-12">
										    <label for="partyName">Party Name</label>
										    <div class="input-group">
												<div class="input-group-prepend">
									                <span id="party-search" class="btn btn-primary input-group-text"><i class="fab fa-sistrix"></i></span>
									             </div>
											    <select type="text" class="form-control" id="partyName" placeholder="Party Name">
											    </select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-lg-1">
									<div class="row">
										<div style="padding-left: 40px;" class="col-lg-12 form-check">
										  <input class="form-check-input" type="radio" name="voucherTypeRadios" id="PType" value="P">
										  <label class="text-center" style="background-color: #EE0505;color:#fff;padding:2px;border-radius: 3px;width:80px;" class="form-check-label" for="PType" checked>
										    Payment
										  </label>
										</div>
										<div style="padding-left: 40px;margin-top: 1px" class="col-lg-12 form-check">
										  <input class="form-check-input" type="radio" name="voucherTypeRadios" id="RType" value="R" >
										  <label class="text-center" style="background-color: #00A504;color:#fff;padding:2px;border-radius: 3px;width:80px;" class="form-check-label" for="RType">
										    Receipt
										  </label>
										</div>
										
									</div>
								</div>
								<div class="col-sm-12 col-lg-4 offset-lg-1">
									<div class="row">
										<div class="form-group col-lg-12">
										    <label for="particulars">Particulars</label>
										    <div class="input-group">
										    	<div class="input-group-prepend">
									                <span id="particulars-search" class="btn btn-primary input-group-text"><i class="fab fa-sistrix"></i></span>
									             </div>
											    <select type="text" class="form-control" id="particulars" placeholder="Particulars">
											    </select>
											</div>
										  </div>
										  <div class="form-group col-lg-12">
										    <label for="particulars" id="dOrcLabel">Debit</label>
										    <input type="text" class="form-control" id="debitOrCredit" placeholder="Amount in Rs.">
										  </div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
								    <label for="led_narration"><b>Ledger Narration : </b></label>
								     <label style = "color:red" id="led_narration_var"></label>
							  	</div>
								
							</div>
							<div class="row">
								<div class="col-lg-11">
								    <label for="day_narration"><b>DayBook Narration : </b></label>
								     <label style = "color:red" id="day_narration_var"></label>
							  	</div>
							  	<div class="col-lg-1">
									<button class="btn btn-success" id="btn-save">Save</button>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-sm-12">
						
					</div>
				</div>
				<!-- <div class="col-sm-12 col-lg-8">
					<div id="party-grid" class="table-responsive"></div>
				</div> -->
			</div>
		</div>
	</body>
</html>