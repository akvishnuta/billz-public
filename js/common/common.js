$.urlParam = function(name){
    var results = [];
    var resultMap = {};
    var url = window.location.href;
    var secondPart = '';
    secondPart= url.split('?')[1];
    if(secondPart!==undefined){
        if(secondPart!==''){
            results = secondPart.split('&');    
        }    
    }
    
    for(r in results){
        var splitResult = r.split('=');
        resultMap[splitResult[0]] = splitResult[1];
    }
    //results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if(name in resultMap){
        //return results[1] || 0;    
        return resultMap[name];
    }else{
        return 0;
    }
    
}