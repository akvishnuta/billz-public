var typingTimer;                //timer identifier
var doneTypingInterval = 1000;
var addNewOption;
/*
	table : db_table
	colList : list of db columns to be listed in the search grid
	pattern : search keyword(can be empty string to display all data)
	compareCol : db colum to apply the pattern
	codeId : html target element id for the Code
	nameId : html target element id for the Name (can be empty Srting in case inputType='combo')
	inpuutType : html target element type ('text' or 'combo')
*/
var doCommonSearch = function(table,colList,pattern,compareCol,codeId,nameId,inputType,addNewOption){
        var deferred = $.Deferred();
        self.addNewOption = undefined;
        $('#common-search-ok-btn').attr('hidden','true');
        self.addNewOption = addNewOption;
	$('#searchText').val(pattern);
        $.Deferred().resolve().then(function(){
            return doCommonSearchLoad(table,colList,pattern,compareCol,codeId,nameId,inputType);
        }).then(function(count){
            if(count===0 && addNewOption!==undefined){
                $('#common-search-ok-btn').removeAttr('hidden');
                $('#common-search-footer').on('click','#common-search-ok-btn',function(event){
                    event.preventDefault();
                    addNewOption($('#searchText').val());
                });
            }else{
                $('#common-search-ok-btn').attr('hidden','true');
                return $.Deferred().resolve();
            }
        }).then(function(){
            deferred.resolve();
        });
        return deferred.promise();
};

var doCommonSearchLoad = function(table,colList,pattern,compareCol,codeId,nameId,inputType){
	var deferred = $.Deferred();
	$('#splash-screen').show();
	$('#common-search-grid').html('Loading...Please wait!');
        $('#row-count').html('');
	var rowCount = 0;
	var dataMap = {};
	dataMap.table = table==null?'':table;
	dataMap.colList = colList==null?'':colList;
	dataMap.pattern = pattern==null?'':pattern;
	dataMap.compareCol = compareCol==null?'':compareCol;
	dataString ='dataString='+JSON.stringify(dataMap);
	$.ajax({
    type:'POST',
    data:dataString,
    url:'commonSearch/doLoad',
    success:function(data) {
    	if(data === 'Failed'){
    		$('#common-search-grid').html('Failed to load. Please try again later!');
    		$('#splash-screen').hide();
                deferred.resolve(0);
    	}else{
    		$('#splash-screen').hide();
    		//load grid here
    		dataObj = JSON.parse(data);
    		if(dataObj.length==0){
    			$('#common-search-grid').html('No results');
    		}else{
	    		$('#common-search-grid').html('');
	    		$('#common-search-grid').append('<table class="table-hover table table-bordered table-striped" id="common-search-dynamic-table">');
	    		$('#common-search-dynamic-table').append('<thead><tr id="common-search-dynamic-header">');
	    		$('#common-search-dynamic-table').append('<tbody id="common-search-dynamic-body" >');
	    		for(var row=0;row<dataObj.length;row++){
	    			$('#common-search-dynamic-body').append('<tr class="clickable-row" id="cs-row'+row+'">');
	    			var rowName = '#cs-row'+row;
	    			var col=0;
	    			for(item in dataObj[row]){
	    				col++;
	    				$(rowName).append('<td class="cs-col'+col+'">'+dataObj[row][item]);
	    			}
	    		}
	    	}
                if(dataObj.length===1){
                    $('#row-count').html('1 row');
                }else{
                    $('#row-count').html(dataObj.length+' rows');
                }
                
    	}
    	$('#common-search-dynamic-table').on('click', '.clickable-row', function(event) {
		  $(this).addClass('active').siblings().removeClass('active');
		  var id = $('#'+event.currentTarget.id +' .cs-col1').text();
		  var name = $('#'+event.currentTarget.id +' .cs-col2').text();
		  if(inputType==='text'){
			  if(codeId!==''){
			  	$('#'+codeId).val(id);
			  }
			  if(nameId!==''){
			  	$('#'+nameId).val(name);
			  }
		  }else if(inputType==='combo'){
		  	if(codeId!==''){
			  	$('#'+codeId).val(id).change();
                                var changedComboText = $('#'+codeId+' option:selected').html();
                                if(changedComboText!==name){
                                    if(changedComboText!==''){
                                        $('#common-modal').modal('show');
                                        $('#common-modal-title').empty();
                                        $('#common-modal-title').append('<span>Failed!</span>');
                                        $('#common-modal-body').empty();
                                        $('#common-modal-body').append('<p>An Unexpected <b style="color:">Error</b> occured! Please try <strong style="color:green">refreshing</strong> the page to avoid consitency problems.</p>');

                                    }else{
                                        $('#'+codeId).append('<option value="'+id+'">'+name+'</option>');
                                        $('#'+codeId).val(id).change();
                                    }
                                }
			 }
		  }
		  
		  $('#common-search-modal').modal('hide');
		});

                
   		$('#searchText').on('keyup', '', function(event) {
   			clearTimeout(typingTimer);
  			typingTimer = setTimeout(function(){
  				var pattern = $('#searchText').val();
		  		doCommonSearch(table,colList,pattern,compareCol,codeId,nameId,inputType,addNewOption);
  			}, doneTypingInterval);
		});
		$('#searchText').on('keydown', '', function(event) {
   			clearTimeout(typingTimer);
		});
                deferred.resolve(dataObj.length);
	    }
	  });

	return deferred.promise();
};