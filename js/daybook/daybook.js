var partyList={};
var areaList={};
var totalCredit=0;
var totalDebit =0;
var total=0;
$(document).ready(function(){
    loadAreaSelect();
  $("#totalCredit").prop("readonly", true);
  $("#totalDebit").prop("readonly", true);
  $("#total").prop("readonly", true);
  $('#openingBal').prop("readonly",true);
	clearTexts();
    $("#btn-load").click(function(e){
   		e.preventDefault();
		  var dateFrom = $('#date-from').val();
    	var dateTo = $('#date-to').val();
    	//var area = $('#area').val();
      var area = 'Ernakulam';
      loadPartyList();
      doLoad(dateFrom,dateTo,area);
    });
    
});



$('#common-modal').on('show.bs.modal', function (event) {
  var modal = $(this);
  $('#ok-btn').focus();
});

var showError = function(msg){
	$('#error-label').html(msg);
}

var loadAreaSelect = function(){
    $('#splash-screen').show();
    var methodName = 'loadAreaSelect';
    var dataString = 'methodName='+methodName;
    $.ajax({
    type:'POST',
    data:dataString,
    url:'dayBook/loadAreas',
    success:function(data) {
        if(data === 'Failed'){
            alert('An Unexpected error occured in our server! Contact the administrator');
            $('#splash-screen').hide();
        }else{
            /*$('#billNo').val(data);*/
            $('#area').html('');
            dataObj = JSON.parse(data);
            areaList={};
            $('#area').append('<option value="-1">All</option>');
            areaList['-1'] = 'All';
            for(var row=0;row<dataObj.length;row++){
                $('#area').append('<option value="'+dataObj[row].code+'">'+dataObj[row].area+'</option>');
                areaList[dataObj[row].code] = dataObj[row].area;
            }
            $('#splash-screen').hide();
        }
        
    }
  });
};
var formatDate = function(strDate){
  var dateObj=new Date(strDate);
  var day = dateObj.getDate()+'';
  var month = dateObj.getMonth() + 1+'';
  var year = dateObj.getFullYear()+'';
  year = year.substring(2, 4);
  if(month.length<2){
    month='0'+month;
  }
  if(day.length<2){
    day='0'+day;
  }
  var newDateStr = day+'-'+month+'-'+year;
  return newDateStr;
}
var doLoad = function(dateFrom,dateTo,area){
  $('#splash-screen').show();
  var dataMap={};
  dataMap.dateFrom = dateFrom;
  dataMap.dateTo = dateTo;
  dataMap.area = areaList[$('#area').val()];
  dataString = 'dataString='+JSON.stringify(dataMap);
	$.ajax({
    type:'POST',
    data:dataString,
    url:'dayBook/doLoad',
    success:function(response) {
        var data = JSON.parse(response);
        var dataObj = data.dataObj;
    	if(data.status === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		$('#splash-screen').hide();
    		//load grid here
        totalCredit = 0;
        totalDebit = 0;
        total=0;
        if (dataObj.length==0){
          clearCalcTexts();
          $('#daybook-grid').html('No records found!');
        }else{
      		$('#daybook-grid').html('');
          var tempDate='';
          for(var row=0;row<dataObj.length;row++){
            if(tempDate!==dataObj[row].date1){
                dataObj[row].date1 =  dataObj[row].date1;
                tempDate = dataObj[row].date1;
            }else{
                dataObj[row].date1 = '';  
            }
            totalDebit = totalDebit+parseInt(dataObj[row].Debit);
            totalCredit = totalCredit+parseInt(dataObj[row].Credit);
            $('#totalCredit').val(totalCredit);
            $('#totalDebit').val(totalDebit);
      		}
                
        $("#daybook-grid").jsGrid({
                    width: "100%",
                    height: "400px",

                    inserting: false,
                    editing: false,
                    sorting: true,
                    paging: true,

                    data: dataObj,

                    fields: [
                        { name: "Date", type: "text", width: 100},
                        { name: "Narration", type: "text", width: 250 },
                        { name: "Debit", type: "text", width: 50 },
                        { name: "Credit", type: "text", width: 50 },
                        { name: "Login", type: "text", width: 100 },
                        { name: "Area", type: "text", width: 100 },
                        { name: "VNo", type: "text", width: 75 },
                        { name: "partyCode", type: "text", width: 75 }
                    ]
                });
                        
          total = totalDebit-totalCredit+data.openingBal;
          $('#total').val(total);
          $('#openingBal').val(data.openingBal);
      	}
      }
   		
	    }
	  });
};

var loadPartyList = function(filter){
  $('#splash-screen').show();
  var methodName = 'loadPartyList';
  var dataString = 'methodName='+methodName;
  $.ajax({
    type:'POST',
    data:dataString,
    url:'dayBook/loadPartyList',
    success:function(data) {
      if(data === 'Failed'){
        alert('An Unexpected error occured in our server! Contact the administrator');
        $('#splash-screen').hide();
      }else{
        partyList={};
        dataObj = JSON.parse(data);
        for(var row=0;row<dataObj.length;row++){
          partyList[dataObj[row].partyCode] = dataObj[row].partyName;
        }
        $('#splash-screen').hide();
      }
      
    }
  });
};
var clearTexts = function(){
	$('#date-to').val(getCurrentDate());
	$('#date-from').val(getCurrentDate());
	$('#area').val('Ernakulam');
};

var clearCalcTexts = function(){
  $('#openingBal').val(0);
  $('#totalDebit').val(0);
  $('#totalCredit').val(0);
  $('#total').val(0);
};

var getCurrentDate = function(){
  var now = new Date();
  var month = now.getMonth() + 1+'';
  var day = now.getDate()+'';
  if(month.length<2){
    month='0'+month;
  }
  if(day.length<2){
    day='0'+day;
  }
    var today = now.getFullYear()   + '-' + month + '-' + day;
    return today;
};