var slNo=1;
var gridList=[];
var glb_total=0;
$(document).ready(function(){
	$('#sign-in-btn').click(function(e){
		e.preventDefault();
		var username = $('#username').val();
		var password = $('#password').val();
        var areaCode = $('#area').val();
		checkLogin(username,password,areaCode);
	})
    loadAreaSelect();
});

var loadAreaSelect = function(){
    $('#splash-screen').show();
    var methodName = 'loadAreaSelect';
    var dataString = 'methodName='+methodName;
    $.ajax({
    type:'POST',
    data:dataString,
    url:"login/loadAreas",
    success:function(data) {
        if(data === 'Failed'){
            alert('An Unexpected error occured in our server! Contact the administrator');
            $('#splash-screen').hide();
        }else{
            $('#area').html('');
            dataObj = JSON.parse(data);
            $('#area').append('<option value="">Choose Area</option>');
            for(var row=0;row<dataObj.length;row++){
                $('#area').append('<option value="'+dataObj[row].code+'">'+dataObj[row].area+'</option>');
            }
            $('#splash-screen').hide();
        }
        
    }
  });
};

var checkLogin = function(username,password,areaCode){
	$('#login-error').html('');
	$('#splash-screen').show();
	var dataString ='';
	var dataMap = {};
	dataMap['username'] = username;
	dataMap['password'] = password;
        dataMap['areaCode'] = areaCode;
	dataString = 'dataString='+JSON.stringify(dataMap);
	$.ajax({
    type:'POST',
    data:dataString,
    url:'login/checkUser',
    success:function(response) {
    	checkResult = JSON.parse(response);
    	if(response === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		if(checkResult.status==='true'){
    			//var redir_url = $.urlParam('redir');
                var redir_url = checkResult.redir_url;
                if (redir_url!==''){
                    window.location = redir_url;    
                }else{
                    window.location = 'home';
                }
                
    		}else{
    			$('#login-error').html('Invalid username or password or area!');
    		}
    		$('#splash-screen').hide();
    	}
   		
    }
  });
}
