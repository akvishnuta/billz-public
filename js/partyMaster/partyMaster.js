var dataList = [];
$(document).ready(function(){
	
	addNew();
	doLoad();
    $("#btn-save").click(function(e){
   	e.preventDefault();
        var partyCode = $('#partyCode').val();
    	var partyName = $('#partyName').val();
    	var partyMark = $('#partyMark').val();
        var partyAddress = $('#partyAddress').val();
        var gstNo = $('#gstNo').val();
    	//initiating ajax call
    	if(validateBeforeSave(partyCode,partyName,partyMark,partyAddress,gstNo)){
    		doSave(partyCode,partyName,partyMark,partyAddress,gstNo);
    	}
    	
    });
    
});

$('#common-modal').on('show.bs.modal', function (event) {
  var modal = $(this);
  $('#ok-btn').focus();
});
var validateBeforeSave =function(partyCode,partyName,partyMark,partyAddress,gstNo){
	var checkFlag=true;
	showError('');
	if(partyCode==='0' || partyCode===''){
		showError("Party Code cannot be blank!");
		checkFlag = false;
	}else if(partyName===''){
		showError("Party Name cannot be blank!");
		checkFlag = false;
	}else if(partyMark==='' || partyMark===null){
		showError("Party Mark cannot be blank!");
		checkFlag = false;
	}
	return checkFlag;
};
var showError = function(msg){
	$('#error-label').html(msg);
}
var doSave = function(partyCode,partyName,partyMark,partyAddress,gstNo){
	$('#splash-screen').show();
        var dataMap = {};
        dataMap['partyCode'] = partyCode;
        dataMap['partyName'] = partyName;
        dataMap['partyMark'] = partyMark;
        dataMap['partyAddress'] = partyAddress;
        dataMap['gstNo'] = gstNo;
	$.ajax({
    type:'POST',
    data:'dataString='+JSON.stringify(dataMap),
    url:'partyMaster/doSave',
    success:function(response) {
            var result = JSON.parse(response);
   		if(result.status==='Saved'){
   			$('#common-modal').modal('show');
   			$('#common-modal-title').empty();
   			$('#common-modal-title').append('<span>Successfully Saved!</span>');
   			$('#common-modal-body').empty();
   			$('#common-modal-body').append('<p>PartyCode : '+result.partyCode+'<br>'
                                                        +'PartyName : '+result.partyName+'</p>');
   		}else{
   			$('#common-modal').modal('show');
   			$('#common-modal-title').empty();
   			$('#common-modal-title').append('<span>Failed!</span>');
   			$('#common-modal-body').empty();
   			$('#common-modal-body').append('<p>An unexpected error occured and we are unable to complete your request.</p>');
   			$('#common-modal-body').append('<p>Sorry for the inconvenience!</p>');
   		}

   		$('#splash-screen').hide();
   		clearTexts();
   		addNew();
   		doLoad();
    }
  });
};

var addNew = function(){
	clearTexts();
	$('#splash-screen').show();
	$("#partyCode").prop("readonly", true);
	var dataString = '';
	$.ajax({
    type:'POST',
    data:dataString,
    url:'partyMaster/addNew',
    success:function(data) {
    	if(data === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		$('#partyCode').val(data);
    		$('#splash-screen').hide();
    		$('#partyName').focus();	
    	}
   		
    }
  });
};

var doLoad = function(){
	$('#splash-screen').show();
        var methodName = 'loadAreaSelect';
        var dataString = 'methodName='+methodName;
	$.ajax({
    type:'POST',
    data:dataString,
    url:"partyMaster/doLoad",
    success:function(data) {
    	if(data === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		
    		//load grid here
                var headerMap={
                    partyCode:'Code',
                    partyName:'Name',
                    partyMark:'Mark',
                    partyAddress:'Address',
                    gstNo:'GST No.'
                }
    		dataObj = JSON.parse(data);
    		$('#party-grid').html('');
    		dataList = dataObj;
                
                        $("#party-grid").jsGrid({
                            width: "100%",
                            height: "500px",

                            inserting: false,
                            editing: false,
                            sorting: true,
                            paging: true,
                            selecting:true,

                            data: dataList,

                            fields: [
                                { title:headerMap.partyCode,name: "partyCode", type: "text", width: 100},
                                { title:headerMap.partyName,name: "partyName", type: "text", width: 200 },
                                { title:headerMap.partyMark,name: "partyMark", type: "text", width: 150 },
                                { title:headerMap.partyAddress,name: "partyAddress", type: "text", width: 150, visible:false },
                                { title:headerMap.gstNo, name: "gstNo", type: "text", width: 50, visible : false }
                            ],
                           rowClick: function(args) {
                                console.log(args)
                                var contents='';
                                var getData = args.item;
                                var keys = Object.keys(getData);
                                $('#common-modal').modal('show');
                                $('#common-modal-title').html('Details : '+getData.partyName);
                                contents='<table>';
                                $.each(keys,function(index,key){
                                    var value = getData[key]!==undefined && getData[key]!==null?getData[key]:'';
                                    contents+=  '<tr>'+'<td>'+headerMap[key]+'</td><td> : </td>'+'<td> <b>'+value+'</b></td>'+'</tr>';
                                });   
                                contents+='</table>';
                                $('#common-modal-body').html(contents);
                                
                              }
                        });
            $('#splash-screen').hide();
    	}
   		
	    }
	  });
};

var clearTexts = function(){
	$('#partyCode').val('');
	$('#partyName').val('');
	$('#partyMark').val('P');
};