var slNo=1;
var gridList=[];
var glb_total=0;
var modified=[];
var files;
var username;
$(document).ready(function(){
    doBind();
    loadAreaSelect();
});
var loadProfile = function(){
    $('#splash-screen').show();
    var methodName = 'loadProfile';
    var dataString = 'methodName='+methodName;
    $.ajax({
    type:'POST',
    data:dataString,
    url:"profile/loadProfile",
    success:function(data) {
        if(data === 'Failed'){
            alert('An Unexpected error occured in our server! Contact the administrator');
            $('#splash-screen').hide();
        }else{
            dataObj = JSON.parse(data);
            $('#name-display').html(dataObj[0].name);
            username = dataObj[0].username;
            $('#username-display').html('Username : '+ username);
            $('#name').val(dataObj[0].name);
            $('#phone').val(dataObj[0].phone);
            $('#email').val(dataObj[0].email);
            $('#area').val(dataObj[0].areaCode);
            if(dataObj[0].imgurl!=='' || dataObj[0].imgUrl!==null || dataObj[0].imgUrl!==undefined){
                $('#profile-photo').attr('src',dataObj[0].imgurl);
            }
            $('#splash-screen').hide();
        }
        
    }
    });
};
var uploadImg = function(files){
    var data = new FormData();
    $('#splash-screen').show();
    data.append('userfile',files[0]);
    $.ajax({
            type: 'POST',
            url: 'profile/doUpload',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                
            },
            success: function(msg){
                $('#splash-screen').hide();
                saveImgUrl();
            }
        });
};
var saveImgUrl = function(){
    var data = {};
    $('#splash-screen').show();
    if(modified.indexOf('file')!==-1){
        data.filename = files[0].name;
        data.username = username;
        dataString = 'dataString='+JSON.stringify(data);
        $.ajax({
        type:'POST',
        data:dataString,
        url:'profile/doSaveImgUrl',
        success:function(response) {
            result = JSON.parse(response);
            if(result.status==='Saved'){
                //alert('success');
            }else{
                //alert('failed');
            }
            loadProfile();
            $('#splash-screen').hide();
        }
        });
    }
}
var doBind = function(){
    $('#change-btn').click(function(){
        $('#file').trigger('click');
    });
    $('#file').change(function(event){
        $('#common-modal').modal('show');
        $('#common-modal-title').html('Change Profile Picture');
        $('#common-modal-body').html('Are you sure you want to change the profile picture?');
        $('#yes-btn').removeAttr('hidden');
        $('#no-btn').removeAttr('hidden');
        $('#yes-btn').click(function(){
           uploadImg(event.target.files);
           files = event.target.files;
           modified.push('file');
        });
    });
    $('#update-btn').click(function(e){
            e.preventDefault();
            var username = $('#username').val();
            var password = $('#password').val();
            var retypePassword = $('#retype-password').val();
            var phone = $('#phone').val();
            var areaCode = $('#area').val();
            error = validate(username,password,retypePassword,phone,areaCode); 
            if(error.checkFlag){
                $('#login-error').html('');
                doSave(username,password,phone,areaCode);
            }else{
                switch(error.type){
                    case 'retypePassword':
                        $('#login-error').html('Retype Passwords does not match');
                        break;
                    default:
                        $('#login-error').html('An unknown error occured!');
                }
                
            }
	});
}
var validatePassword = function(username,pssword,newPassword,retypePassword){
    error={};
    error['type']='';
    error['checkFlag']=true;
    
    if(newPassword!==retypePassword){
        error.type = 'retypePassword';
        error.checkFlag=false;
    }
    return error;
}
var loadAreaSelect = function(){
    $('#splash-screen').show();
    var methodName = 'loadAreaSelect';
    var dataString = 'methodName='+methodName;
    $.ajax({
    type:'POST',
    data:dataString,
    url:"profile/loadAreas",
    success:function(data) {
        if(data === 'Failed'){
            alert('An Unexpected error occured in our server! Contact the administrator');
            $('#splash-screen').hide();
        }else{
            $('#area').html('');
            dataObj = JSON.parse(data);
            $('#area').append('<option value="">Choose Area</option>');
            $('#area').append('<option value="-1">All Area</option>');
            for(var row=0;row<dataObj.length;row++){
                $('#area').append('<option value="'+dataObj[row].code+'">'+dataObj[row].area+'</option>');
            }
            loadProfile();
        }
        
    }
  });
};

var doSave = function(username,name,phone,email,imgUrl,areaCode,password,newPassword,tab){
	$('#login-error').html('');
	$('#splash-screen').show();
	var dataString ='';
	var dataMap = {};
	dataMap['username'] = username;
	dataMap['name'] = name;
        dataMap['phone'] = phone;
        dataMap['email'] = email;
        dataMap['imgUrl'] = imgUrl;
        dataMap['areaCode'] = areaCode;
        dataMap['password'] = password;
        dataMap['newPassword'] = newPassword;
        dataMap['tab'] = tab;
	dataString = 'dataString='+JSON.stringify(dataMap);
	$.ajax({
        type:'POST',
        data:dataString,
        url:'profile/doSave',
        success:function(response) {
            result = JSON.parse(response);
            if(result.status==='Saved'){
                alert('success');
            }else{
                alert('failed');
            }
            clearTexts();
            $('#splash-screen').hide();
        }
  });
};

var clearTexts = function(){
    $('#username').val('');
    $('#password').val('');
    $('#retype-password').val('');
    $('#phone').val('');
    //$('#area').val('1');
};