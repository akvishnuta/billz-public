var slNo=1;
var gridList=[];
var glb_total=0;
$(document).ready(function(){
    doBind();
    loadAreaSelect();
});
var doBind = function(){
    $('#register-btn').click(function(e){
            e.preventDefault();
            var username = $('#username').val();
            var password = $('#password').val();
            var retypePassword = $('#retype-password').val();
            var phone = $('#phone').val();
            var areaCode = $('#area').val();
            error = validate(username,password,retypePassword,phone,areaCode); 
            if(error.checkFlag){
                $('#login-error').html('');
                doSave(username,password,phone,areaCode);
            }else{
                switch(error.type){
                    case 'retypePassword':
                        $('#login-error').html('Retype Passwords does not match');
                        break;
                    default:
                        $('#login-error').html('An unknown error occured!');
                }
                
            }
	});
}
var validate = function(username,password,retypePassword,phone,areaCode){
    error={};
    error['type']='';
    error['checkFlag']=true;
    
    if(password!==retypePassword){
        error.type = 'retypePassword';
        error.checkFlag=false;
    }
    return error;
}
var loadAreaSelect = function(){
    $('#splash-screen').show();
    var methodName = 'loadAreaSelect';
    var dataString = 'methodName='+methodName;
    $.ajax({
    type:'POST',
    data:dataString,
    url:"register/loadAreas",
    success:function(data) {
        if(data === 'Failed'){
            alert('An Unexpected error occured in our server! Contact the administrator');
            $('#splash-screen').hide();
        }else{
            $('#area').html('');
            dataObj = JSON.parse(data);
            $('#area').append('<option value="">Choose Area</option>');
            $('#area').append('<option value="-1">All Area</option>');
            for(var row=0;row<dataObj.length;row++){
                $('#area').append('<option value="'+dataObj[row].code+'">'+dataObj[row].area+'</option>');
            }
            $('#splash-screen').hide();
        }
        
    }
  });
};

var doSave = function(username,password,phone,areaCode){
	$('#login-error').html('');
	$('#splash-screen').show();
	var dataString ='';
	var dataMap = {};
	dataMap['username'] = username;
	dataMap['password'] = password;
        dataMap['phone'] = phone;
        dataMap['areaCode'] = areaCode;
	dataString = 'dataString='+JSON.stringify(dataMap);
	$.ajax({
        type:'POST',
        data:dataString,
        url:'register/doSave',
        success:function(response) {
            result = JSON.parse(response);
            if(result.status==='Saved'){
                alert('success');
            }else{
                alert('failed');
            }
            clearTexts();
            $('#splash-screen').hide();
        }
  });
};

var clearTexts = function(){
    $('#username').val('');
    $('#password').val('');
    $('#retype-password').val('');
    $('#phone').val('');
    $('#area').val('1');
};