var slNo=1;
var gridList=[];
var glb_total=0;
var itemRateMap={};
$(document).ready(function(){
	$('#slNo').val(slNo);
	$('#qty').val(0);
	$('#rate').val(0);
	$('#total').val(0);
	$('#partyName').on('keyup','',function(e){
		//$('#partyCode').val($('#partyName').val());
		$('#partyCode').val('');
	});

	$(document).on("click", function(e) {
	  var classList = e.target.classList;
	  if(classList.contains('empty-btn')){
		  	var delRow = e.target.id.split('-')[1]
		  	gridList.splice(delRow-1,1);
		  	slNo = slNo-1;
		  	$('#slNo').val(slNo);
		  	reorderGrid();
		  	updateGrid();

	  	}
	  	if(classList.contains('edit-btn')){
		  	var editRow = e.target.id.split('-')[1]
		  	var editRowId = '#row'+editRow;
		  	$('#edit-modal').modal('show');
		  	addValuesToEditModal(editRowId,editRow);
	  	}
	});
	$('#btn-save').click(function(e){
	   		e.preventDefault();
			var billNo = $('#billNo').val();
	    	var partyName = $('#partyName').val();
	    	var partyCode = $('#partyCode').val();
	    	var date = $('#date').val();
	    	//initiating ajax call
	    	if(validateBeforeSave(billNo,partyName,partyCode,date)){
	    		doSave(billNo,partyName,partyCode,date);
	    	}
	    });

	$('#edit-ok-btn').click(function(e){
	   		e.preventDefault();
			changeToGrid();
	    });
	$('#party-search').click(function(e){
	   		e.preventDefault();
	   		$('#common-search-modal').modal('show');
	   		var table = 'partymaster';
	   		var colList = ['partyCode','partyName'];
	   		var pattern = $('#partyName').val();
	   		var compareCol = 'partyName';
	   		var codeId = 'partyCode';
	   		var nameId = 'partyName';
	   		var inputType = 'text';
	   		doCommonSearch(table,colList,pattern,compareCol,codeId,nameId,inputType);
	    });
	$('#item-search').click(function(e){
	   		e.preventDefault();
	   		$('#common-search-modal').modal('show');
	   		var table = 'item_mas';
	   		var colList = ['itemCode','itemName'];
	   		var pattern = '';
	   		var compareCol = 'itemName';
	   		var codeId = 'item';
	   		var nameId = '';
	   		var inputType = 'combo';
	   		doCommonSearch(table,colList,pattern,compareCol,codeId,nameId,inputType);
	    });

	$('#item').change(function(e){
		var value = $('#item').val();
		var rate = itemRateMap[value][0];
		$('#rate').val(rate);
		if($('#item').val()!=='' || $('#item').val()!==null){
			$('#qty').val('1');
		}
		if(($('#item').val()==='' || $('#item').val()===null)){
			$('#qty').val('0');
		}
		if($('#rate').val()===''){
			$('#rate').val('0');
		}
		calculateTotal();

		
	});

	$('#edit-item').change(function(e){
		var value = $('#edit-item').val();
		var rate = itemRateMap[value][0];
		$('#edit-rate').val(rate);
		if($('#edit-item').val()!=='' || $('#edit-item').val()!==null){
			$('#edit-qty').val('1');
		}
		if(($('#edit-item').val()==='' || $('#edit-item').val()===null)){
			$('#edit-qty').val('0');
		}
		if($('#edit-rate').val()===''){
			$('#edit-rate').val('0');
		}
		calculateTotalInEdit();

		
	});
	
var doSave = function(billNo,partyName,partyCode,date){
	$('#splash-screen').show();
	var dataMap = {};
	dataMap['billNo'] = billNo;
	dataMap['partyName'] = partyName;
	dataMap['partyCode'] = partyCode;
	dataMap['date'] = date;
	dataMap['total'] = glb_total;
	dataMap['gridList'] = gridList;
	dataString ='dataString='+JSON.stringify(dataMap);
	$.ajax({
    type:'POST',
    data:dataString,
    url:'saleBill/doSave',
    success:function(response) {
        var result = JSON.parse(response);
   		if(result.status==='Saved'){
   			$('#common-modal').modal('show');
   			$('#common-modal-title').empty();
   			$('#common-modal-title').append('<span>Successfully Saved!</span>');
   			$('#common-modal-body').empty();
   			$('#common-modal-body').append('<p>BillNo : '+result.billNo+'<br>'
                                                        +'PartyName : '+result.partyName+'</p>');
   		}else{
   			$('#common-modal').modal('show');
   			$('#common-modal-title').empty();
   			$('#common-modal-title').append('<span>Failed!</span>');
   			$('#common-modal-body').empty();
   			$('#common-modal-body').append('<p>An unexpected error occured and we are unable to complete your request.</p>');
   			$('#common-modal-body').append('<p>Sorry for the inconvenience!</p>');
   		}

   		$('#splash-screen').hide();
   		clearTexts();
   		clearGrid();
   		addNewBill();

    }
  });
};

	$('#add-btn').click(function(e){
		e.preventDefault();
		var sl= $('#slNo').val();
		var itemVal= $('#item').val().split(':')[0];
		var item=$('#item option:selected').text();
		var itemCode=$('#item').val();
		var qty= $('#qty').val();
		var rate= $('#rate').val();
		var total= $('#total').val();
		if(validateBeforeGrid(sl,itemVal,qty,rate,total)){
			addToGrid(sl,item,itemCode,qty,rate,total);	
		}
	});
	$('.qty').focus(function(){
		if($(this).val()==='0'){
			$(this).val('');
		}
	});
	$('.rate').focus(function(){
		if($(this).val()==='0'){
			$(this).val('');
		}
	});
	$('.qty').focusout(function(){
		if($(this).val()===''){
			$(this).val('0');
		}
	});
	$('.rate').focusout(function(){
		if($(this).val()===''){
			$(this).val('0');
		}
	});
	$('#qty').keyup(function(e){
		calculateTotal();
	});
	$('#rate').keyup(function(e){
		calculateTotal();
	});
	$('#edit-qty').keyup(function(e){
		calculateTotalInEdit();
	});
	$('#edit-rate').keyup(function(e){
		calculateTotalInEdit();
	});
	addNewBill();
	setCurrentDate();
});
var setCurrentDate = function(){
	var now = new Date();
	var month = now.getMonth() + 1+'';
	var day = now.getDate()+'';
	if(month.length<2){
		month='0'+month;
	}
	if(day.length<2){
		day='0'+day;
	}
    var today = now.getFullYear()   + '-' + month + '-' + day;
    $('#date').val(today);
};
var addNewBill = function(){
	//clearTexts();
	$('#splash-screen').show();
	$("#partyCode").prop("readonly", true);
	$.ajax({
    type:'POST',
    data:'',
    url:'saleBill/addNew',
    success:function(data) {
    	if(data === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		$('#billNo').val(data);
    		$('#splash-screen').hide();
    		$('#partyName').focus();	
    	}
   		//loadPartyCombo();
   		loadItemCombo();
    }
  });
}
var loadPartyCombo = function(){
	$('#splash-screen').show();
	$("#partyCode").prop("readonly", true);
	var methodName = 'loadPartyCombo';
	var dataString = 'methodName='+methodName;
	$.ajax({
    type:'POST',
    data:dataString,
    url:'php/saleBill/RI/saleBillRI.php',
    success:function(data) {
    	if(data === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		/*$('#billNo').val(data);*/
    		$('#partyName').html('');
    		dataObj = JSON.parse(data);
    		$('#partyName').append('<option value="">Choose Party</option>');
    		for(var row=0;row<dataObj.partyCode.length;row++){
    			$('#partyName').append('<option value="'+dataObj.partyCode[row]+'">'+dataObj.partyName[row]+'</option>');
    		}
    		$('#splash-screen').hide();
    		$('#partyName').focus();	
    	}
   		
    }
  });
};
var loadItemCombo = function(){
	$('#splash-screen').show();
	$("#partyCode").prop("readonly", true);
	$.ajax({
    type:'POST',
    data:'',
    url:'saleBill/loadItems',
    success:function(data) {
    	if(data === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		/*$('#billNo').val(data);*/
    		$('#item').html('');
    		$('#edit-item').html('');
    		dataObj = JSON.parse(data);

    		$('#item').append('<option value="">Choose Item</option>');
    		$('#edit-item').append('<option value="">Choose Item</option>');
    		itemRateMap={};
    		for(var row=0;row<dataObj.length;row++){
    			itemRateMap[dataObj[row].itemCode] = [dataObj[row].rate,dataObj[row].itemName];
    			$('#item').append('<option value="'+dataObj[row].itemCode+'">'+dataObj[row].itemName+'</option>');
    			$('#edit-item').append('<option value="'+dataObj[row].itemCode+'">'+dataObj[row].itemName+'</option>');
    		}
    		$('#splash-screen').hide();
    	}
   		
    }
  });
};
var calculateTotal = function(){
	var total = 0;
	var qty= $('#qty').val();
	var rate= $('#rate').val();
	total = qty * rate;
	$('#total').val(total);
}
var calculateTotalInEdit = function(){
	var total = 0;
	var qty= $('#edit-qty').val();
	var rate= $('#edit-rate').val();
	total = qty * rate;
	$('#edit-total').val(total);
}
var validateBeforeGrid = function(sl,item,qty,rate,total){
	showError('');
	if(sl==='0' || sl==='' || sl===null ){
		showError('slNo cannot be 0 or empty');
		return false;
	}else if(item==='' || item===null){
		showError('item cannot be blank or null');
		return false;
	}else if(qty==='0' || qty==='' || qty===null ){
		showError('Qty cannot be 0 or empty');
		return false;
	}else if(rate==='0' || rate==='' || rate===null ){
		showError('Rate cannot be 0 or empty');
		return false;
	}else if(total==='0' || total==='' || total===null ){
		showError('Total cannot be 0 or empty');
		return false;
	}
	return true;
}
var addToGrid = function(sl,item,itemCode,qty,rate,total){
	var girdRow = {};
	showError('');
	if(isNotDuplicate(itemCode)){
		girdRow['sl'] = sl;
		girdRow['itemCode'] = itemCode;
		girdRow['qty'] = qty;
		girdRow['rate'] = rate;
		girdRow['total'] = total;
		gridList.push(girdRow);
		updateGrid();
		clearTempTexts();
		slNo+=1;
		$('#slNo').val(slNo);
	}else{
		showError('Item already added,<br>please consider editing the below!');
	}
	
}
var calculateGlobalTotal = function(){
	var total = 0;
	glb_total=0;
	for(sl=0;sl<gridList.length;sl++){
		total = gridList[sl]['total'];
		glb_total+=parseFloat(total);
	}
	$('#grand-total').val(glb_total);
};
var isNotDuplicate = function(itemCode){
	for(sl=0;sl<gridList.length;sl++){
		if(gridList[sl]['itemCode']==itemCode){
			return false;
		}
	}
	return true;
};
var updateGrid = function(){
	$('#dynamic-body').html('');
	for(sl=0;sl<gridList.length;sl++){
		$('#dynamic-body').append('<tr id="row'+gridList[sl]['sl']+'"">');
		var rowName = '#row'+gridList[sl]['sl'];
		$(rowName).append('<td class="sl-col">'+gridList[sl]['sl']+'</td>');
		$(rowName).append('<td class="item-col">'+itemRateMap[gridList[sl]['itemCode']][1]+'</td>');
		$(rowName).append('<td class="qty-col">'+gridList[sl]['qty']+'</td>');
		$(rowName).append('<td class="rate-col">'+gridList[sl]['rate']+'</td>');
		$(rowName).append('<td class="total-col">'+gridList[sl]['total']+'</td>');
		$(rowName).append('<td class="option-col"><span class="fas fa-trash-alt empty-btn" id="empty-'+gridList[sl]['sl']+'"></span>&nbsp;&nbsp;&nbsp;<span class="fas fa-edit edit-btn" id="edit-'+gridList[sl]['sl']+'"></span></td>');	
	}
		calculateGlobalTotal();	
}
var reorderGrid = function(){
	for (var i = 0; i<gridList.length; i++) {
		gridList[i]['sl'] = (i+1)+'';
	}
	updateGrid();
}

var showMainError = function(msg){
	$('#error-label-main').html(msg);
}

var showError = function(msg){
	$('#error-label').html(msg);
}

var clearTempTexts = function(){
	$('#slNo').val('');
	$('#item').val('');
	$('#qty').val('');
	$('#rate').val('');
	$('#total').val('');
}

var clearTexts = function(){
	$('#billNo').val('');
	$('#partyName').val('');
	$('#partyCode').val('');
	$('#partyName').val('');
	$('#grand-total').val('');
	setCurrentDate();
}

var clearGrid = function(){
	$('#dynamic-body').html('');
	gridList=[];
	slNo=1;
	$('#slNo').val(slNo);
}

var validateBeforeSave = function(billNo,partyName,partyCode,date){
	showMainError('');
	if(billNo==='' || billNo===undefined){
		showMainError('Bill No cannot be blank');
		return false;
	}
	if(partyName==='' || partyName===undefined){
		showMainError('PartyName cannot be blank');
		return false;
	}
	if(partyCode==='' || partyCode===undefined){
		showMainError('PartyCode cannot be blank');
		return false;
	}
	if(date==='' || date===undefined){
		showMainError('Date Field cannot be blank');
		return false;
	}
	if(gridList.length>0){
		return true;
	}else{
		$('#common-modal').modal('show');
		$('#common-modal-title').empty();
		$('#common-modal-title').append('<span>Error</span>');
		$('#common-modal-body').empty();
		$('#common-modal-body').append('<p>No Item is added to your cart! Please Select Item(s) to continue!</p>');
		return false;
	}
	return true;
}
var addValuesToEditModal = function(editRowId,editRowNo){
	var editRow = $(editRowId);
	var tdList = editRow.find('td');
	$('#edit-slNo').val(tdList[0].innerText);
	$('#edit-item').val(gridList[editRowNo-1]['itemCode']);
	$('#edit-qty').val(tdList[2].innerText);
	$('#edit-rate').val(tdList[3].innerText);
	$('#edit-total').val(tdList[4].innerText);
}
var changeToGrid = function(){
	edit_slNo = $('#edit-slNo').val();
	edit_item_code = $('#edit-item').val();
	edit_qty = $('#edit-qty').val();
	edit_rate = $('#edit-rate').val();
	edit_total = $('#edit-total').val();
	for(sl=0;sl<gridList.length;sl++){
		if(gridList[sl]['sl']==edit_slNo){
			gridList[sl]['itemCode'] = edit_item_code;
			gridList[sl]['qty'] = edit_qty;
			gridList[sl]['rate'] = edit_rate;
			gridList[sl]['total'] = edit_total;
			break;
		}
	}
	updateGrid();
}