var dOrCVal = 'd';
var particularList={};
var partyList = {};
var led_narration_var='';
var day_narration_var='';
$(document).ready(function(){
    addNew();
    $("#btn-save").click(function(e){
   		e.preventDefault();
		  var voucherNo = $('#voucherNo').val();
    	var date = $('#date').val();
    	var partyCode = $('#partyName').val();
      var RorP = dOrCVal==='d'?'R':'P';
      var particulars = particularList[$('#particulars').val()];
      var debit = dOrCVal==='d'?$('#debitOrCredit').val():'0';
      var credit = dOrCVal==='c'?$('#debitOrCredit').val():'0';

    	//initiating ajax call
    	if(validateBeforeSave(voucherNo,date,partyCode,RorP,particulars,debit,credit)){
    		doSave(voucherNo,date,partyCode,RorP,particulars,debit,credit);
    	}  	
    });
    $('#RType').click(function(e){
      $('#dOrcLabel').html('Debit');
      dOrCVal = 'd';
      prepareNarration();
    });
    $('#PType').click(function(e){
      $('#dOrcLabel').html('Credit');
      dOrCVal = 'c';
      prepareNarration();
    });
    $('#partyName').change(function(){
      prepareNarration();
    });
    $('#particulars').change(function(){
      prepareNarration();
    });
    $('#debitOrCredit').keyup(function(){
      prepareNarration();
    });
    $('#party-search').click(function(e){
        e.preventDefault();
        $('#common-search-modal').modal('show');
        var table = 'partymaster';
        var colList = ['partyCode','partyName'];
        var pattern = '';
        var compareCol = 'partyName';
        var codeId = 'partyName';
        var nameId = '';
        var inputType = 'combo';
        var deferred = $.Deferred();
        $.Deferred().resolve().then(function(){
            return doCommonSearch(table,colList,pattern,compareCol,codeId,nameId,inputType);
        }).then(function(){
            deferred.resolve();
      });
        return deferred.promise();
      });
    $('#particulars-search').click(function(e){
        e.preventDefault();
        $('#common-search-modal').modal('show');
        var table = 'particulars';
        var colList = ['sl','item'];
        var pattern = '';
        var compareCol = 'item';
        var codeId = 'particulars';
        var nameId = '';
        var inputType = 'combo';
        var addNewOption = true;
        var deferred = $.Deferred();
        var addNewOption = function(searchText){
            particularList['-1'] = searchText;
            addToParticularsCombo();
            $('#particulars').val(-1).change();
        };
        $.Deferred().resolve().then(function(){
           return doCommonSearch(table,colList,pattern,compareCol,codeId,nameId,inputType,addNewOption);
        }).then(function(){
            deferred.resolve();
      });
        return deferred.promise();
      });
    $('#PType').trigger('click');
});

var loadPartyCombo = function(filter){
  $('#splash-screen').show();
  $("#partyCode").prop("readonly", true);
  $.ajax({
    type:'POST',
    data:'',
    url:'voucher/loadPartyCombo',
    success:function(data) {
      if(data === 'Failed'){
        alert('An Unexpected error occured in our server! Contact the administrator');
        $('#splash-screen').hide();
      }else{
        partyList={};
        $('#partyName').html('');
        dataObj = JSON.parse(data);
        $('#partyName').append('<option value="">Choose Party</option>');
        for(var row=0;row<dataObj.length;row++){
          $('#partyName').append('<option value="'+dataObj[row].partyCode+'">'+dataObj[row].partyName+'</option>');
          partyList[dataObj[row].partyCode] = dataObj[row].partyName;
        }
        $('#splash-screen').hide();
        $('#partyName').focus();  
      }
      
    }
  });
};
var loadParticulars = function(){
  $('#splash-screen').show();
  $.ajax({
    type:'POST',
    data:'',
    url:'voucher/loadParticulars',
    success:function(data) {
      if(data === 'Failed'){
        alert('An Unexpected error occured in our server! Contact the administrator');
        $('#splash-screen').hide();
      }else{
        particularList = {};
        dataObj = JSON.parse(data);
        addToParticularsCombo();
        $('#splash-screen').hide();
      }
      
    }
  });
};
var addToParticularsCombo = function(){
    $('#particulars').html('');
    $('#particulars').append('<option value="">Choose Particulars</option>');
    if(particularList[-1]!==undefined){
        $('#particulars').append('<option value="-1">'+particularList[-1]+'</option>');
    }
    for(var row=0;row<dataObj.length;row++){
        particularList[dataObj[row].sl] = dataObj[row].item;
        $('#particulars').append('<option value="'+dataObj[row].sl+'">'+dataObj[row].item+'</option>');
    }
};
$('#common-modal').on('show.bs.modal', function (event) {
  var modal = $(this);
  $('#ok-btn').focus();
});

var setCurrentDate = function(){
  var now = new Date();
  var month = now.getMonth() + 1+'';
  var day = now.getDate()+'';
  if(month.length<2){
    month='0'+month;
  }
  if(day.length<2){
    day='0'+day;
  }
    var today = now.getFullYear()   + '-' + month + '-' + day;
    $('#date').val(today);
};

var validateBeforeSave =function(voucherNo,date,partyCode,RorP,particulars,debit,credit){
	var checkFlag=true;
	/*showError('');
	if(partyCode==='0' || partyCode===''){
		showError("Party Code cannot be blank!");
		checkFlag = false;
	}else if(partyName===''){
		showError("Party Name cannot be blank!");
		checkFlag = false;
	}else if(partyMark==='' || partyMark===null){
		showError("Party Mark cannot be blank!");
		checkFlag = false;
	}*/
	return checkFlag;
};
var showError = function(msg){
	$('#error-label').html(msg);
}
var doSave = function(voucherNo,date,partyCode,RorP,particulars,debit,credit){
  $('#splash-screen').show();
  var dataMap = {};
  dataMap['voucherNo']=voucherNo;
  dataMap['date']=date;
  dataMap['partyCode']=partyCode;
  dataMap['partyName']=partyList[parseInt(partyCode)];
  dataMap['RorP']=RorP;
  dataMap['particulars']=particulars;
  dataMap['debit']=debit;
  dataMap['credit']=credit;
  dataMap['led_narr']=led_narration_var;
  dataMap['day_narr']=day_narration_var;
	var dataString = '&dataString='+JSON.stringify(dataMap);
	$.ajax({
    type:'POST',
    data:dataString,
    url:'voucher/doSave',
    success:function(response) {
        var result = JSON.parse(response);
        if(result.status==='Saved'){
                $('#common-modal').modal('show');
                $('#common-modal-title').empty();
                $('#common-modal-title').append('<span>Successfully Saved!</span>');
                $('#common-modal-body').empty();
                $('#common-modal-body').append('<p>VNo. : '+result.vno+'<br>'
                                                +'Date : '+result.date+'<br>'
                                                +'PartyName : '+result.partyName+'</p>');
                $('#ok-btn').removeAttr('hidden');                        
        }else{
                $('#common-modal').modal('show');
                $('#common-modal-title').empty();
                $('#common-modal-title').append('<span>Failed!</span>');
                $('#common-modal-body').empty();
                $('#common-modal-body').append('<p>An unexpected error occured and we are unable to complete your request.</p>');
                $('#common-modal-body').append('<p>Sorry for the inconvenience!</p>');
                $('#ok-btn').removeAttr('hidden');
        }

        $('#splash-screen').hide();
        clearTexts();
        addNew();
    }
  });
};

var addNew = function(){
	clearTexts();
	$('#splash-screen').show();
	$("#partyCode").prop("readonly", true);
	$.ajax({
    type:'POST',
    data:'',
    url:'voucher/addNew',
    success:function(data) {
    	if(data === 'Failed'){
    		alert('An Unexpected error occured in our server! Contact the administrator');
    		$('#splash-screen').hide();
    	}else{
    		$('#voucherNo').val(data);
    		$('#splash-screen').hide();
    		$('#partyName').focus();	
        loadPartyCombo();
        loadParticulars();
        setCurrentDate();
    	}
    }
  });
};
var clearTexts = function(){
	$('#voucherNo').val('');
	$('#date').val('');
	$('#partyCode').val('');
  $( "#PType" ).trigger( "click" );
  $('#particulars').val('');
  $('#debitOrCredit').val('');
  $('#led_narration_var').html('');
  $('#day_narration_var').html('');
};

var prepareNarration = function(){
  var particular = particularList[$('#particulars').val()];
  if(particular===undefined){
      particular = $('#particulars option:selected').html();
  }
  var partyName = partyList[parseInt($('#partyName').val())];
  if(partyName===undefined){
      partyName = $('#partyName option:selected').html();
  }
  var debitOrCredit = $('#debitOrCredit').val();
  if(dOrCVal==='d'){
    led_narration_var='Rec. '+particular;
    day_narration_var='Rec. '+partyName+' '+particular;
    $('#led_narration_var').html(led_narration_var);
    $('#day_narration_var').html(day_narration_var);  
  }else{
    led_narration_var='Paid '+particular;
    day_narration_var='Paid '+partyName+' '+particular;
    $('#led_narration_var').html(led_narration_var);
    $('#day_narration_var').html(day_narration_var);  
  }
  
};