###################
What is Billz
###################

Billz is a GST billing web application.
The web app provided here with is a basic model for GST billing.
This PHP application uses CodeIgniter as a backend framework.
Bootstrap is used as the front end framework.
The app further uses open source technologies such as jsgrid for achieving
various functionalities.

*********
@Author
*********
This project was developed by 
Akhil Kumar T A

***************
About the Author
***************
Akhil is an Electronics and communication engineer who is currently working as a
software developer.

